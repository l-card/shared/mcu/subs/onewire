#include "onewire_ops.h"
#include "onewire_config.h"
#include "onewire_signaling.h"
#include "onewire_dev.h"
#include <stddef.h>

#ifndef ONEWIRE_OP_QUEUE_SIZE
    #define ONEWIRE_OP_QUEUE_SIZE    32
#endif
#define ONEWIRE_OP_QUEUE_MSK  (ONEWIRE_OP_QUEUE_SIZE - 1)

typedef enum {
    ONEWIRE_OP_RST,
    ONEWIRE_OP_WR,
    ONEWIRE_OP_RD,
    ONEWIRE_OP_WT_PAUSE, /* ожидание заданной паузы в нс */
    ONEWIRE_OP_SET_DEF_SPEED, /* установка скорости интерфейса по умолчанию для всех устройств */
    ONEWIRE_OP_SET_CUR_SPEED, /* установка только текущей скорости без изменения скорости по умолчанию */
    ONEWIRE_OP_SET_STRONG_HIGH, /* установить режим сильной "1" на линии */
    ONEWIRE_OP_CALLBACK
} t_onewire_op;

typedef enum {
    ONEWIRE_OPSTATE_IDLE,
    ONEWIRE_OPSTATE_SIG_WAIT,
    ONEWIRE_OPSTATE_DONE
} t_onewire_opstate;

typedef struct {
    uint8_t  opcode;
    uint32_t bitsize;
    union {
        uint8_t *rx_data;
        const uint8_t *tx_data;
        uint32_t time_ns;
        bool on;
        struct {
            t_onewire_ops_finish_cb cb;
            void *cb_data;
        };
        struct {
            t_onewire_speed speed;
            const t_onewire_rom_cmd *rom;
        };
    };
} t_ow_op;

typedef struct {
    t_ow_op ops[ONEWIRE_OP_QUEUE_SIZE];
    unsigned op_get_pos;
    unsigned op_put_pos;
    struct {
        t_onewire_opstate state;
        uint32_t bitpos;
        t_onewire_err err;
    } cur_op;
    t_onewire_speed def_speed; /* скорость интерфейса по умолчанию на момент последней операции в очереди */
    t_onewire_speed cur_speed; /* скорость интерфейса на момент последней операции в очереди */
} t_ow_iface_state;


static t_ow_iface_state f_iface_ops[ONEWIRE_CFG_IFACE_CNT];


static void op_exec(int iface) {
    t_ow_iface_state *state = &f_iface_ops[iface];
    t_ow_op *op = &state->ops[state->op_get_pos];
    if (op->opcode == ONEWIRE_OP_RST) {
        if (state->cur_op.state == ONEWIRE_OPSTATE_SIG_WAIT) {
            state->cur_op.state = ONEWIRE_OPSTATE_DONE;
        } else {
            /* по началу сброса сбрасывается и ошибка интерфейса */
            state->cur_op.err = ONEWIRE_ERR_OK;
            state->cur_op.state = ONEWIRE_OPSTATE_SIG_WAIT;
            onewire_sig_start_detect(iface);
        }
    } else if (op->opcode == ONEWIRE_OP_WR) {
        if (state->cur_op.state == ONEWIRE_OPSTATE_SIG_WAIT) {
            state->cur_op.bitpos++;
        }

        if (state->cur_op.bitpos == op->bitsize) {
            state->cur_op.state = ONEWIRE_OPSTATE_DONE;
        } else {
            const unsigned bytepos = state->cur_op.bitpos >> 3;
            const unsigned bitmsk  = 1U << (state->cur_op.bitpos & 0x7);
            state->cur_op.state = ONEWIRE_OPSTATE_SIG_WAIT;
            onewire_sig_start_bit_wr(iface, !!(op->tx_data[bytepos] & bitmsk));
        }
    } else if (op->opcode == ONEWIRE_OP_RD) {
        if (state->cur_op.state == ONEWIRE_OPSTATE_SIG_WAIT) {
            const unsigned bytepos = state->cur_op.bitpos >> 3;
            const unsigned bitmsk  = 1U << (state->cur_op.bitpos & 0x7);

            if (onweire_sig_rd_bit(iface)) {
                op->rx_data[bytepos] |= bitmsk;
            } else {
                op->rx_data[bytepos] &= ~bitmsk;
            }
            state->cur_op.bitpos++;
        }
        if (state->cur_op.bitpos == op->bitsize) {
            state->cur_op.state = ONEWIRE_OPSTATE_DONE;
        } else {
            state->cur_op.state = ONEWIRE_OPSTATE_SIG_WAIT;
            onewire_sig_start_bit_rd(iface);
        }
    } else if (op->opcode == ONEWIRE_OP_WT_PAUSE) {
        if (state->cur_op.state == ONEWIRE_OPSTATE_SIG_WAIT) {
            state->cur_op.state = ONEWIRE_OPSTATE_DONE;
        } else {
            state->cur_op.state = ONEWIRE_OPSTATE_SIG_WAIT;
            onewire_sig_start_pause_ns(iface, op->time_ns);
        }
    } else if (op->opcode == ONEWIRE_OP_SET_STRONG_HIGH) {
        onewire_sig_set_strong_high_mode(iface, op->on);
        state->cur_op.state = ONEWIRE_OPSTATE_DONE;
    } else if (op->opcode == ONEWIRE_OP_SET_DEF_SPEED) {
        state->cur_op.state = ONEWIRE_OPSTATE_DONE;
        onewire_sig_set_speed(iface, op->speed);
    } else if (op->opcode == ONEWIRE_OP_SET_CUR_SPEED) {
        state->cur_op.state = ONEWIRE_OPSTATE_DONE;
        onewire_sig_set_speed(iface, op->speed);
    } else if (op->opcode == ONEWIRE_OP_CALLBACK) {
        state->cur_op.state = ONEWIRE_OPSTATE_DONE;
        op->cb(iface, op->cb_data);
    }
}

static inline t_ow_op *next_put_op(int iface) {
    t_ow_iface_state *state = &f_iface_ops[iface];
    unsigned next_put = (state->op_put_pos + 1) & ONEWIRE_OP_QUEUE_MSK;
    return (next_put != state->op_get_pos) ? &state->ops[state->op_put_pos] : NULL;
}

static inline void put_op_finish(int iface) {
    t_ow_iface_state *state = &f_iface_ops[iface];
    unsigned put_pos = state->op_put_pos;
    state->op_put_pos = (put_pos + 1) & ONEWIRE_OP_QUEUE_MSK;
}

static inline void get_op_finish(int iface) {
    t_ow_iface_state *state = &f_iface_ops[iface];
    unsigned put_pos = state->op_get_pos;
    state->op_get_pos = (put_pos + 1) & ONEWIRE_OP_QUEUE_MSK;
    state->cur_op.bitpos = 0;
    state->cur_op.state = ONEWIRE_OPSTATE_IDLE;
}

void onewire_ops_init(void) {
    for (int iface = 0; iface < ONEWIRE_CFG_IFACE_CNT; ++iface) {
        t_ow_iface_state *state = &f_iface_ops[iface];
        state->def_speed = state->cur_speed = ONEWIRE_SPEED_STD;
        state->op_put_pos = state->op_get_pos = 0;
        state->cur_op.state = ONEWIRE_OPSTATE_IDLE;
        state->cur_op.err = ONEWIRE_ERR_OK;
        onewire_sig_set_speed(iface, ONEWIRE_SPEED_STD);
    }

}

void onewire_ops_poll(void) {
    for (int iface = 0; iface < ONEWIRE_CFG_IFACE_CNT; ++iface) {
        if (onewire_ops_busy(iface)) {
            t_ow_iface_state *state = &f_iface_ops[iface];
            if (state->cur_op.state == ONEWIRE_OPSTATE_SIG_WAIT) {
                if (onewire_sig_rdy(iface)) {
                    state->cur_op.err = onewire_sig_err(iface);
                    if (!state->cur_op.err) {
                        op_exec(iface);
                    } else {
                        /* отмена всех дальнейших операций */
                        state->op_put_pos = state->op_get_pos;
                        state->cur_op.state = ONEWIRE_OPSTATE_IDLE;
                    }
                }
            } else if (state->cur_op.state == ONEWIRE_OPSTATE_DONE) {
                get_op_finish(iface);
                if (onewire_ops_busy(iface)) {
                    op_exec(iface);
                }
            } else if (state->cur_op.state == ONEWIRE_OPSTATE_IDLE) {
                op_exec(iface);
            }
        }
    }
}


bool onewire_ops_busy(int iface) {
    const t_ow_iface_state *state = &f_iface_ops[iface];
    return state->op_get_pos != state->op_put_pos;
}

t_onewire_err onewire_ops_err(int iface) {
    const t_ow_iface_state *state = &f_iface_ops[iface];
    return state->cur_op.err;
}

void onewire_ops_set_err(int iface, t_onewire_err err) {
    f_iface_ops[iface].cur_op.err = err;
}


t_onewire_speed onewire_ops_def_speed(int iface) {
    const t_ow_iface_state *state = &f_iface_ops[iface];
    return state->def_speed;
}

t_onewire_speed onewire_ops_cur_speed(int iface) {
    const t_ow_iface_state *state = &f_iface_ops[iface];
    return state->cur_speed;
}


void onewire_ops_add_write_bits(int iface, const uint8_t *data, uint32_t bitsize) {
    t_ow_op *op = next_put_op(iface);
    if (op) {
        op->bitsize = bitsize;
        op->opcode = ONEWIRE_OP_WR;
        op->tx_data = data;
        put_op_finish(iface);
    }
}

void onewire_ops_add_read_bits(int iface, uint8_t *data, uint32_t bitsize) {
    t_ow_op *op = next_put_op(iface);
    if (op) {
        op->bitsize = bitsize;
        op->opcode = ONEWIRE_OP_RD;
        op->rx_data = data;
        put_op_finish(iface);
    }
}

void onewire_ops_add_rst_det(int iface) {
    t_ow_op *op = next_put_op(iface);
    if (op) {
        op->opcode = ONEWIRE_OP_RST;
        /* сброс на STD-скорости сбрасывает OVD скорость всех устройств */
        if (onewire_ops_cur_speed(iface) == ONEWIRE_SPEED_STD)
            onewire_devs_clr_speed(iface);
        put_op_finish(iface);
    }
}

void onewire_ops_add_pause_ns(int iface, uint32_t time_ns) {
    t_ow_op *op = next_put_op(iface);
    if (op) {
        op->opcode = ONEWIRE_OP_WT_PAUSE;
        op->time_ns = time_ns;
        put_op_finish(iface);
    }
}

void onewire_ops_add_set_strong_high_mode(int iface, bool on) {
    t_ow_op *op = next_put_op(iface);
    if (op) {
        op->opcode = ONEWIRE_OP_SET_STRONG_HIGH;
        op->on = on;
        put_op_finish(iface);
    }
}



void onewire_ops_add_set_def_speed(int iface, t_onewire_speed speed) {
    t_ow_op *op = next_put_op(iface);
    if (op) {
        op->opcode = ONEWIRE_OP_SET_DEF_SPEED;
        op->speed = speed;
        f_iface_ops[iface].def_speed = f_iface_ops[iface].cur_speed  = speed;
        put_op_finish(iface);
    }
}

void onewire_ops_add_set_cur_speed(int iface, t_onewire_speed speed) {
    t_ow_op *op = next_put_op(iface);
    if (op) {
        op->opcode = ONEWIRE_OP_SET_CUR_SPEED;
        op->speed = speed;
        f_iface_ops[iface].cur_speed = speed;
        put_op_finish(iface);
    }
}


void onewire_ops_add_finish_cb(int iface, t_onewire_ops_finish_cb cb, void *cb_data) {
    t_ow_op *op = next_put_op(iface);
    if (op) {
        op->opcode = ONEWIRE_OP_CALLBACK;
        op->cb = cb;
        op->cb_data = cb_data;
        put_op_finish(iface);
    }
}

t_onewire_err onewire_ops_wait_iface_finish(int iface, t_onewire_ops_poll_app_cb cb) {
    while (onewire_ops_busy(iface)) {
        onewire_ops_poll();
        if (cb != NULL) {
            cb();
        }
    }
    return onewire_ops_err(iface);
}
