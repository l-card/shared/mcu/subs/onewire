#ifndef ONEWIRE_DEFS_H
#define ONEWIRE_DEFS_H

#include <stdint.h>
#include <stdbool.h>

typedef  enum {
    ONEWIRE_ERR_OK                  = 0,
    ONEWIRE_ERR_PD_LINE_LOW         = -1,
    ONEWIRE_ERR_PD_NOT_DETECTED     = -2,
    ONEWIRE_ERR_CRC_MISMATCH        = -3,


    ONEWIRE_ERR_DEV_ROM_NOT_VALID   = -19,
    ONEWIRE_ERR_DEV_INVALID_ADDR    = -20,
    ONEWIRE_ERR_DEV_INVALID_SIZE    = -21,
    ONEWIRE_ERR_DEV_NOT_ROW_ALIGN   = -22,
    ONEWIRE_ERR_DEV_STATUS_CHECK    = -23,
    ONEWIRE_ERR_DEV_DATA_CHECK      = -24,
    ONEWIRE_ERR_DEV_DONE_PATTERN    = -25
} t_onewire_err;

typedef enum {
    ONEWIRE_SPEED_STD = 0,   /* Standard speed */
    ONEWIRE_SPEED_OVDRV = 1  /* Overdrive speed */
} t_onewire_speed;


#endif // ONEWIRE_DEFS_H
