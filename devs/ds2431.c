#include "ds2431.h"
#include "onewire_ops.h"
#include "onewire_romcmd.h"
#include <string.h>

static void f_wr_scratch_cpy_finish(int iface, void *ctx);
static void f_wr_scratch_rd_finish(int iface, void *ctx);

void ds2431_read_start(t_onewire_dev *dev, uint16_t addr, uint8_t *data, uint16_t size, t_ds2431_rd_ctx *rd_ctx) {

    if ((addr + size) > DS2431_MEM_ADDR_MAX) {
        onewire_ops_set_err(onewire_dev_iface_num(dev), ONEWIRE_ERR_DEV_INVALID_ADDR);
    } else if (size == 0) {
        onewire_ops_set_err(onewire_dev_iface_num(dev), ONEWIRE_ERR_DEV_INVALID_SIZE);
    } else {
        onewire_dev_romsel_start(dev);

        int tx_pos = 0;
        rd_ctx->buf[tx_pos++] = DS2431_CMD_READ_MEMORY;
        rd_ctx->buf[tx_pos++] = addr & 0xFF;
        rd_ctx->buf[tx_pos++] = (addr >> 8) & 0xFF;
        onewire_ops_add_write(onewire_dev_iface_num(dev), rd_ctx->buf, tx_pos);
        onewire_ops_add_read(onewire_dev_iface_num(dev), data, size);
    }
}


static void f_wr_start_row(t_ds2431_wr_ctx *wr_ctx) {
    onewire_dev_romsel_start(wr_ctx->dev);
    int tx_pos = 0;
    wr_ctx->buf[tx_pos++] = DS2431_CMD_WRITE_SCRATCHPAD;
    wr_ctx->buf[tx_pos++] = wr_ctx->cur_addr & 0xFF;
    wr_ctx->buf[tx_pos++] = (wr_ctx->cur_addr >> 8) & 0xFF;
    for (int i = 0; i < DS2431_MEM_ROW_SIZE; ++i) {
        wr_ctx->buf[tx_pos++] = wr_ctx->cur_data[i];
    }
    onewire_ops_add_write(onewire_dev_iface_num(wr_ctx->dev), wr_ctx->buf, tx_pos);

    onewire_dev_romsel_start(wr_ctx->dev);
    int start_pos = tx_pos;
    wr_ctx->buf[tx_pos++] = DS2431_CMD_READ_SCRATCHPAD;
    onewire_ops_add_write(onewire_dev_iface_num(wr_ctx->dev), &wr_ctx->buf[start_pos], tx_pos - start_pos);
    onewire_ops_add_read(onewire_dev_iface_num(wr_ctx->dev), wr_ctx->buf, DS2431_MEM_ROW_SIZE + 3);
    onewire_ops_add_finish_cb(onewire_dev_iface_num(wr_ctx->dev), f_wr_scratch_rd_finish, wr_ctx);
}


static void f_wr_scratch_cpy_finish(int iface, void *ctx) {
    t_ds2431_wr_ctx *wr_ctx = (t_ds2431_wr_ctx *)ctx;
    uint8_t rd_bits = wr_ctx->buf[0] & 3;
    if (rd_bits != 0x02) {
        onewire_ops_set_err(iface, ONEWIRE_ERR_DEV_DONE_PATTERN);
    } else {
        wr_ctx->cur_addr += DS2431_MEM_ROW_SIZE;
        wr_ctx->rem_size -= DS2431_MEM_ROW_SIZE;
        wr_ctx->cur_data += DS2431_MEM_ROW_SIZE;
        if (wr_ctx->rem_size != 0) {
            f_wr_start_row(wr_ctx);
        }
    }
}

static void f_wr_scratch_rd_finish(int iface, void *ctx) {
    t_ds2431_wr_ctx *wr_ctx = (t_ds2431_wr_ctx *)ctx;
    uint16_t addr = (((uint16_t)wr_ctx->buf[1] << 8) | wr_ctx->buf[0]);
    uint8_t  status_reg = wr_ctx->buf[2];


    if ((addr != wr_ctx->cur_addr)
            || (status_reg & DS2431_STATUS_FLD_AA)
            || (status_reg & DS2431_STATUS_FLD_PF)
            ) {
        onewire_ops_set_err(iface, ONEWIRE_ERR_DEV_STATUS_CHECK);

    } else if (memcmp(wr_ctx->cur_data, &wr_ctx->buf[3], DS2431_MEM_ROW_SIZE) != 0) {
        onewire_ops_set_err(iface, ONEWIRE_ERR_DEV_DATA_CHECK);
    } else {
        onewire_dev_romsel_start(wr_ctx->dev);
        int tx_pos = 0;
        wr_ctx->buf[tx_pos++] = DS2431_CMD_COPY_SCRATCHPAD;
        wr_ctx->buf[tx_pos++] = addr & 0xFF;
        wr_ctx->buf[tx_pos++] = (addr >> 8) & 0xFF;
        wr_ctx->buf[tx_pos++] = status_reg;
        onewire_ops_add_write(iface, wr_ctx->buf, tx_pos);
        onewire_ops_add_set_strong_high_mode(iface, true);
        onewire_ops_add_pause_ns(iface, DS2431_MEM_PROG_TIME_NS);
        onewire_ops_add_set_strong_high_mode(iface, false);
        onewire_ops_add_read_bits(iface, wr_ctx->buf, 2);
        onewire_ops_add_finish_cb(iface, f_wr_scratch_cpy_finish, wr_ctx);
    }
}

void ds2431_write_start(t_onewire_dev *dev, uint16_t addr, const uint8_t *data,
                        uint16_t size, t_ds2431_wr_ctx *wr_ctx) {
    const int iface = onewire_dev_iface_num(dev);
    if ((addr + size) > DS2431_MEM_ADDR_MAX) {
        onewire_ops_set_err(iface, ONEWIRE_ERR_DEV_INVALID_ADDR);
    } else if (size == 0) {
        onewire_ops_set_err(onewire_dev_iface_num(dev), ONEWIRE_ERR_DEV_INVALID_SIZE);
    } else if (((addr & DS2431_MEM_ROW_ADDR_MSK) != 0)  || (((addr + size) & DS2431_MEM_ROW_ADDR_MSK) != 0)) {
        onewire_ops_set_err(iface, ONEWIRE_ERR_DEV_NOT_ROW_ALIGN);
    } else {
        wr_ctx->cur_addr = addr;
        wr_ctx->rem_size = size;
        wr_ctx->cur_data = data;
        wr_ctx->dev = dev;

        f_wr_start_row(wr_ctx);
    }
}



t_onewire_err ds2431_read(t_onewire_dev *dev, uint16_t addr, uint8_t *data, uint16_t size, t_onewire_ops_poll_app_cb cb) {
    t_ds2431_rd_ctx ctx;
    ds2431_read_start(dev, addr, data, size, &ctx);
    return onewire_ops_wait_iface_finish(onewire_dev_iface_num(dev), cb);
}

t_onewire_err ds2431_write(t_onewire_dev *dev, uint16_t addr, const uint8_t *data, uint16_t size, t_onewire_ops_poll_app_cb cb) {
    t_ds2431_wr_ctx ctx;
    ds2431_write_start(dev, addr, data, size, &ctx);
    return onewire_ops_wait_iface_finish(onewire_dev_iface_num(dev), cb);
}
