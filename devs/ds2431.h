#ifndef DS2431_H
#define DS2431_H

#include "onewire_dev.h"


#define DS2431_FAMILY_CODE              0x2D

#define DS2431_MEM_SIZE                 128 /* 128 байт = 1024 бит */
#define DS2431_MEM_PAGE_CNT             4
#define DS2431_MEM_PAGE_SIZE            32
#define DS2431_MEM_ROW_SIZE             8
#define DS2431_MEM_ROW_ADDR_MSK         (DS2431_MEM_ROW_SIZE-1)

/* по datasheet время 10мс, но есть ревизия A1, где 12.5.
 * бывают экземляры, где время больше 10 мс, поэтому взято с запасом */
#define DS2431_MEM_PROG_TIME_NS         (30*1000000)

/* адреса в общем адресном пространстве памяти */
#define DS2431_MEM_ADDR_PAGE_DATA(i)    (i * DS2431_MEM_PAGE_SIZE)
#define DS2431_MEM_ADDR_PAGE_PROT(i)    (0x80 + i)
#define DS2431_MEM_ADDR_COPY_PROT(i)    (0x84)
#define DS2431_MEM_ADDR_FACTORY_BYTE    (0x85)
#define DS2431_MEM_ADDR_USER_MAN_ID1    (0x86)
#define DS2431_MEM_ADDR_USER_MAN_ID2    (0x87)
#define DS2431_MEM_ADDR_MAX             (0x8F)

/* коды для записи в MEM_ADDR_PAGE_PROT/MEM_ADDR_COPY_PROT для включения защиты */
#define DS2431_PROTMODE_WRITE_PROT      0x55
#define DS2431_PROTMODE_EEPROM          0xAA


/* Команды работы с памятью DS2431 */
typedef enum {
    DS2431_CMD_WRITE_SCRATCHPAD     = 0x0F,
    DS2431_CMD_READ_SCRATCHPAD      = 0xAA,
    DS2431_CMD_COPY_SCRATCHPAD      = 0x55,
    DS2431_CMD_READ_MEMORY          = 0xF0
} t_ds2431_cmd;

#define DS2431_STATUS_FLD_AA            (0x01 << 7)
#define DS2431_STATUS_FLD_PF            (0x01 << 5)
#define DS2431_STATUS_FLD_E             (0x07 << 0)




typedef struct {
    uint8_t buf[3];
} t_ds2431_rd_ctx;

typedef struct {
    t_onewire_dev *dev;
    uint16_t cur_addr;
    uint16_t rem_size;
    const uint8_t *cur_data;
    uint8_t buf[DS2431_MEM_ROW_SIZE + 6];
} t_ds2431_wr_ctx;

void ds2431_read_start(t_onewire_dev *dev, uint16_t addr, uint8_t *data, uint16_t size, t_ds2431_rd_ctx *rd_ctx);
void ds2431_write_start(t_onewire_dev *dev, uint16_t addr, const uint8_t *data, uint16_t size, t_ds2431_wr_ctx *wr_ctx);

t_onewire_err ds2431_read(t_onewire_dev *dev, uint16_t addr, uint8_t *data, uint16_t size, t_onewire_ops_poll_app_cb cb);
t_onewire_err ds2431_write(t_onewire_dev *dev, uint16_t addr, const uint8_t *data, uint16_t size, t_onewire_ops_poll_app_cb cb);


#endif // DS2431_H
