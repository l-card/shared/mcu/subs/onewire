#ifndef ONEWIRE_OPS_H
#define ONEWIRE_OPS_H

#include <stdint.h>
#include "onewire_defs.h"
#include "onewire_romcmd.h"

typedef void (*t_onewire_ops_finish_cb)(int iface, void *data);
typedef void (*t_onewire_ops_poll_app_cb)(void);

void onewire_ops_init(void);
void onewire_ops_poll(void);

bool onewire_ops_busy(int iface);
t_onewire_err onewire_ops_err(int iface);
t_onewire_speed onewire_ops_def_speed(int iface);
t_onewire_speed onewire_ops_cur_speed(int iface);
void onewire_ops_set_err(int iface, t_onewire_err err);

void onewire_ops_add_rst_det(int iface);
void onewire_ops_add_write_bits(int iface, const uint8_t *data, uint32_t bitsize);
void onewire_ops_add_read_bits(int iface, uint8_t *data, uint32_t bitsize);

static inline void onewire_ops_add_write(int iface, const uint8_t *data, uint32_t size) {
    onewire_ops_add_write_bits(iface, data, size << 3);
}
static inline void onewire_ops_add_read(int iface, uint8_t *data, uint32_t size) {
    onewire_ops_add_read_bits(iface, data, size << 3);
}


void onewire_ops_add_pause_ns(int iface, uint32_t time_ns);
void onewire_ops_add_set_strong_high_mode(int iface, bool on);
void onewire_ops_add_set_def_speed(int iface, t_onewire_speed speed);
void onewire_ops_add_set_cur_speed(int iface, t_onewire_speed speed);
void onewire_ops_add_finish_cb(int iface, t_onewire_ops_finish_cb cb, void *cb_data);

t_onewire_err onewire_ops_wait_iface_finish(int iface, t_onewire_ops_poll_app_cb cb);



#endif // ONEWIRE_OPS_H
