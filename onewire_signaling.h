#ifndef ONEWIRE_SIGNALING_H
#define ONEWIRE_SIGNALING_H

/* Данный файл содержит объявление функций для реализации сигнального уровня
 * однопроводной шины.
 * Реализация данных функций должна быть выполнена в конкретном проекте индивидуально.
 *
 * Интерфейс предполагает возможность выполнения операций на шине в асинхронном фоновом
 * режиме.
 * В этом случае функции onewire_sig_start_start_xxx только запускают обмен
 * и возвращают управление сразу, а о его завершении библиотека узнает
 * с помощью вызова onewire_sig_rdy(),  после чего в случае завершения
 * вызывает onewire_sig_err() для проверки успешности последней операции
 * и onweire_sig_rd_bit() для получения прочитанного значения, если успешно завершилась
 * операция чтения.
 *
 * Однако, если достаточен блокируемый обмен, то функции могут быть реализованы
 * в блокирующем режиме, возвращая управление только по завершению операции.
 * В этом случае onewire_sig_rdy() может всегда возвращать true.
 *
 * Может быть реализовано одновременно несколько интерфейсов однопроводной
 * шины. Для указания номера интерфейса используется параметр iface_num.
 * Конкретная реализация сама определяет соответствие iface_num и конкретной
 * реализации шины. Максимальное кол-во шин определяется настройкой ONEWIRE_CFG_IFACE_CNT.
 * Если используется только одна шина, то iface_num может игнорироваться.
 */
#include "onewire_defs.h"

void onewire_sig_init(void);
void onewire_sig_close(void);
void onewire_sig_set_speed(int iface_num, t_onewire_speed speed);
void onewire_sig_set_strong_high_mode(int iface_num, bool on);
void onewire_sig_start_detect(int iface_num);
void onewire_sig_start_bit_wr(int iface_num, bool val);
void onewire_sig_start_bit_rd(int iface_num);
void onewire_sig_start_pause_ns(int iface_num, uint32_t time_ns);

bool onewire_sig_rdy(int iface_num);
t_onewire_err onewire_sig_err(int iface_num);
bool onweire_sig_rd_bit(int iface_num);



#endif // OW_SIGNALING_H
