#ifndef ONEWIRE_CONFIG_H
#define ONEWIRE_CONFIG_H

/* максимальное количество поддерживаемых интерфейсов однопроводной шины */
#define ONEWIRE_CFG_IFACE_CNT   1
/* максимальное количество поддерживаемых устройств на одной шине */
#define ONEWIRE_CFG_DEV_MAX_CNT 1


#endif // ONEWIRE_CONFIG_H
