#ifndef ONEWIRE_DEVS_H
#define ONEWIRE_DEVS_H

#include "onewire_defs.h"
#include "onewire_romcmd.h"
#include "onewire_ops.h"
#include "onewire_config.h"

typedef enum {
    /* флаг указывает, что устройство одно на шине и можно не выбирать его
     * с момощью MatchRom. Устанавливается автоматом в onewire_dev_add(),
     * если настройка ONEWIRE_CFG_DEV_MAX_CNT == 1, либо вручную,
     * если выяснено, что устройство одно на выбранной шине. */
    ONEWIRE_DEV_FLAG_EXCLUSIVE      =  1 << 0,
    /* Признак, что поле rom устройства действительно и содержит
     * его уникальный идентификатор.
     * Устанавливается автоматом при чтении rom с помощью onewire_dev_romrd() при
     * одном устройстве на шине (определение нескольких сейчас не реализовано) */
    ONEWIRE_DEV_FLAG_DEVROM_VALID   = 1 << 1
} t_onewire_dev_flags;


typedef struct {
#if ONEWIRE_CFG_IFACE_CNT > 1
    int iface_num;
#endif
    t_onewire_speed cur_speed;
    t_onewire_dev_rom rom;
    uint32_t flags;
} t_onewire_dev;


static inline int onewire_dev_iface_num(const t_onewire_dev *dev) {
#if ONEWIRE_CFG_IFACE_CNT > 1
    return dev->iface_num;
#else
    return 0;
#endif
}

void onewire_devs_init(void);
void onewire_devs_clr_speed(int iface);

void onewire_dev_add(t_onewire_dev *dev, int iface_num);
void onewire_dev_set_ovdrv_speed(t_onewire_dev *dev);
void onewire_dev_romrd_exclusive_start(t_onewire_dev *dev);
t_onewire_err onewire_dev_romrd(t_onewire_dev *dev, t_onewire_ops_poll_app_cb cb);
void onewire_dev_romsel_start(t_onewire_dev *dev);

#endif // ONEWIRE_DEVS_H
