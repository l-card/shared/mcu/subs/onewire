#include "onewire_dev.h"
#include "onewire_config.h"
#include <stddef.h>

typedef struct {
    t_onewire_dev *cur_dev;
    t_onewire_dev *devs[ONEWIRE_CFG_DEV_MAX_CNT];
    unsigned reg_devs_cnt;
} t_onewire_iface_devs;

static t_onewire_iface_devs f_iface_devs[ONEWIRE_CFG_IFACE_CNT];

void onewire_devs_init() {
    for (unsigned iface_num = 0; iface_num < ONEWIRE_CFG_IFACE_CNT; ++iface_num) {
        t_onewire_iface_devs *iface_devs = &f_iface_devs[iface_num];
        iface_devs->cur_dev = NULL;
        iface_devs->reg_devs_cnt = 0;
    }
}

void onewire_devs_clr_speed(int iface) {
    for (unsigned iface_num = 0; iface_num < ONEWIRE_CFG_IFACE_CNT; ++iface_num) {
        t_onewire_iface_devs *iface_devs = &f_iface_devs[iface_num];
        for (unsigned dev_num = 0; dev_num < iface_devs->reg_devs_cnt; ++dev_num) {
            iface_devs->devs[dev_num]->cur_speed = ONEWIRE_SPEED_STD;
        }
    }
}



void onewire_dev_add(t_onewire_dev *dev, int iface_num) {
    t_onewire_iface_devs *iface_devs = &f_iface_devs[iface_num];
    if (iface_devs->reg_devs_cnt < ONEWIRE_CFG_DEV_MAX_CNT) {
        dev->cur_speed = ONEWIRE_SPEED_STD;
        dev->flags = 0;
#if ONEWIRE_CFG_DEV_MAX_CNT == 1
        dev->flags |= ONEWIRE_DEV_FLAG_EXCLUSIVE;
#endif
#if ONEWIRE_CFG_IFACE_CNT > 1
        dev->iface_num = iface_num;
#endif
        iface_devs->devs[iface_devs->reg_devs_cnt++] = dev;
    }
}

static void f_dev_cb_fill_ovd_speed(int iface, void *ctx) {
    t_onewire_dev *dev = (t_onewire_dev *)ctx;
    dev->cur_speed = ONEWIRE_SPEED_OVDRV;
}

static void f_dev_romsel_start_speed(t_onewire_dev *dev, t_onewire_speed speed) {
    if (dev->flags & ONEWIRE_DEV_FLAG_EXCLUSIVE) {
        onewire_romcmd_start_skip(onewire_dev_iface_num(dev), speed);
    } else {
        if (dev->flags & ONEWIRE_DEV_FLAG_DEVROM_VALID) {
            t_onewire_iface_devs *iface_devs = &f_iface_devs[onewire_dev_iface_num(dev)];
            if ((iface_devs->cur_dev == dev) && (speed == dev->cur_speed)) {
                onewire_romcmd_start_resume(onewire_dev_iface_num(dev), dev->cur_speed);
            } else {
                iface_devs->cur_dev = dev;
                onewire_romcmd_start_match(onewire_dev_iface_num(dev), &dev->rom, dev->cur_speed, speed);
            }
        } else {
            onewire_ops_set_err(onewire_dev_iface_num(dev), ONEWIRE_ERR_DEV_ROM_NOT_VALID);
        }
    }
}


static void f_exclusive_rom_rd_finish(int iface, void *ctx) {
    t_onewire_dev *dev = (t_onewire_dev *)ctx;
    t_onewire_err err = onewire_romcmd_check_romdata(&dev->rom);
    if (err != ONEWIRE_ERR_OK) {
        onewire_ops_set_err(iface, err);
    } else {
        dev->flags |= ONEWIRE_DEV_FLAG_DEVROM_VALID;
    }
}

void onewire_dev_romrd_exclusive_start(t_onewire_dev *dev) {
    dev->flags &= ~ONEWIRE_DEV_FLAG_DEVROM_VALID;
    dev->flags |= ONEWIRE_DEV_FLAG_EXCLUSIVE;
    onewire_romcmd_start_read(onewire_dev_iface_num(dev), &dev->rom);
    onewire_ops_add_finish_cb(onewire_dev_iface_num(dev), f_exclusive_rom_rd_finish, dev);
}

t_onewire_err onewire_dev_romrd(t_onewire_dev *dev, t_onewire_ops_poll_app_cb cb) {
    onewire_dev_romrd_exclusive_start(dev);
    return onewire_ops_wait_iface_finish(onewire_dev_iface_num(dev), cb);
}

void onewire_dev_romsel_start(t_onewire_dev *dev) {
    f_dev_romsel_start_speed(dev, dev->cur_speed);
}


void onewire_dev_set_ovdrv_speed(t_onewire_dev *dev) {
    f_dev_romsel_start_speed(dev, ONEWIRE_SPEED_OVDRV);
    onewire_ops_add_finish_cb(onewire_dev_iface_num(dev), f_dev_cb_fill_ovd_speed, dev);
}


