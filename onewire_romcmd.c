#include "onewire_romcmd.h"
#include "onewire_ops.h"
#include "onewire_config.h"

static uint8_t onewire_rom_crc(const uint8_t *msg, unsigned msg_len) {
    uint8_t crc = 0;
    uint8_t i;
    for (i = 0; i < msg_len; i++) {
        uint8_t j;
        uint8_t inuint8_t = msg[i];
        for (j = 0; j < 8; j++) {
            uint8_t mix = (crc ^ inuint8_t) & 0x01;
            crc >>= 1;
            if (mix)
                crc ^= 0x8C;

            inuint8_t >>= 1;
        }
    }
    return crc;
}


void onewire_romcmd_start_read(int iface, t_onewire_dev_rom *rom) {
    static const  uint8_t cmd_read_rom = ONWEWIRE_ROMCMD_READ;
    onewire_ops_add_rst_det(iface);
    onewire_ops_add_write(iface, &cmd_read_rom, 1);
    onewire_ops_add_read(iface, rom->data, sizeof(rom->data));
}

t_onewire_err onewire_romcmd_check_romdata(const t_onewire_dev_rom *rom) {
    t_onewire_err err = ONEWIRE_ERR_OK;
    const uint8_t crc = onewire_rom_crc(rom->data, 8);
    if (crc != 0) {
        err = ONEWIRE_ERR_CRC_MISMATCH;
    }
    return err;
}

void onewire_romcmd_start_skip(int iface, t_onewire_speed new_speed) {
    if (new_speed == ONEWIRE_SPEED_STD) {
        /* для STD сразу переводим интерфейс на низкую скорость, чтобы сделать
         * долгий сброс для перевода всех устройств на эту же скорость */
        static const  uint8_t cmd_skip_rom      = ONWEWIRE_ROMCMD_SKIP;
        if (onewire_ops_def_speed(iface) != ONEWIRE_SPEED_STD) {
            onewire_ops_add_set_def_speed(iface, ONEWIRE_SPEED_STD);
        }
        onewire_ops_add_rst_det(iface);
        onewire_ops_add_write(iface, &cmd_skip_rom, 1);
    } else if (new_speed == ONEWIRE_SPEED_OVDRV) {
        static const  uint8_t cmd_skip_rom_ovd  = ONWEWIRE_ROMCMD_OVD_SKIP;
        /* сброс и команда выполняется на текущей скорости */
        onewire_ops_add_rst_det(iface);
        onewire_ops_add_write(iface, &cmd_skip_rom_ovd, 1);
        /* при переходе STD->OVDRV переход идет после выполнения команды */
        if (onewire_ops_def_speed(iface) != ONEWIRE_SPEED_OVDRV) {
            onewire_ops_add_set_def_speed(iface, ONEWIRE_SPEED_OVDRV);
        }
    }

}

void onewire_romcmd_start_match(int iface, const t_onewire_dev_rom *rom, t_onewire_speed cur_speed, t_onewire_speed new_speed) {
    if (new_speed == ONEWIRE_SPEED_STD) {
        static const  uint8_t cmd_match_rom      = ONWEWIRE_ROMCMD_MATCH;

        /* для STD сразу переводим интерфейс на низкую скорость, чтобы сделать
         * долгий сброс для перевода всех устройств на эту же скорость.
         * тут нет возможности перевести только выбранное. */
        if (onewire_ops_cur_speed(iface) != ONEWIRE_SPEED_STD) {
            onewire_ops_add_set_cur_speed(iface, ONEWIRE_SPEED_STD);
        }
        onewire_ops_add_rst_det(iface);
        onewire_ops_add_write(iface, &cmd_match_rom, 1);
        onewire_ops_add_write(iface, rom->data, ONEWIRE_ROM_INFO_SIZE);
    } else {
        static const  uint8_t cmd_math_rom_ovd  = ONWEWIRE_ROMCMD_OVD_MATCH;
        /* на случай, если последний обмен был с устройством на другой скорости, чем выбранное */
        if (onewire_ops_cur_speed(iface) != cur_speed) {
            onewire_ops_add_set_cur_speed(iface, cur_speed);
        }
        /* сброс и команда выполняется на текущей скорости */
        onewire_ops_add_rst_det(iface);
        onewire_ops_add_write(iface, &cmd_math_rom_ovd, 1);
        /* дальнейший обмен, включая передачу ROM, идет уже на OVD скорости */
        if (onewire_ops_cur_speed(iface) != ONEWIRE_SPEED_OVDRV) {
            onewire_ops_add_set_cur_speed(iface, ONEWIRE_SPEED_OVDRV);
        }
        onewire_ops_add_write(iface, rom->data, ONEWIRE_ROM_INFO_SIZE);
    }
}

void onewire_romcmd_start_resume(int iface, t_onewire_speed cur_speed) {
    static const  uint8_t cmd_resume_rom  = ONWEWIRE_ROMCMD_RESUME;
    if (onewire_ops_cur_speed(iface) != cur_speed) {
        onewire_ops_add_set_cur_speed(iface, cur_speed);
    }
    onewire_ops_add_rst_det(iface);
    onewire_ops_add_write(iface, &cmd_resume_rom, 1);
}
