#ifndef ONEWIRE_TIMING_H
#define ONEWIRE_TIMING_H

/* --------------- General Timing --------------------------------------------*/
/* Recovery Time (REC) - только для одного устройства, иначе - больше */
#define ONEWIRE_TIME_STD_REC_MIN         5000
#define ONEWIRE_TIME_OVDRV_REC_MIN       2000
#define ONEWIRE_TIME_OVDRV_REC_RST_MIN   5000 /* prior reset */

/* Rising-Edge Hold-Off Time (std speed only) */
#define ONEWIRE_TIME_STD_REH_MIN          500
#define ONEWIRE_TIME_STD_REH_MAX         5000

/* Time Slot Duration */
#define ONEWIRE_TIME_STD_SLOT_MIN       65000
#define ONEWIRE_TIME_OVDRV_SLOT_MIN      8000

/* --------------- Presence - Detect Cycle -----------------------------------*/
/* Reset Low Time (RSTL) */
#define ONEWIRE_TIME_STD_RSTL_MIN      480000
#define ONEWIRE_TIME_STD_RSTL_MAX      640000
#define ONEWIRE_TIME_OVDRV_RSTL_MIN     48000
#define ONEWIRE_TIME_OVDRV_RSTL_MAX     80000
/* Presence-Detect High Time (PDH) */
#define ONEWIRE_TIME_STD_PDH_MIN        15000
#define ONEWIRE_TIME_STD_PDH_MAX        60000
#define ONEWIRE_TIME_OVDRV_PDH_MIN       2000
#define ONEWIRE_TIME_OVDRV_PDH_MAX       6000
/* Presence-Detect Low Time (PDL) */
#define ONEWIRE_TIME_STD_PDL_MIN        60000
#define ONEWIRE_TIME_STD_PDL_MAX       240000
#define ONEWIRE_TIME_OVDRV_PDL_MIN       8000
#define ONEWIRE_TIME_OVDRV_PDL_MAX      24000
/* Presence-Detect Sample Time (MSP) */
#define ONEWIRE_TIME_STD_MSP_MIN        60000
#define ONEWIRE_TIME_STD_MSP_MAX        75000
#define ONEWIRE_TIME_OVDRV_MSP_MIN       6000
#define ONEWIRE_TIME_OVDRV_MSP_MAX      10000

/* Reset High Time (RSTH) - мин. время после импульса RSTL для совместимости с любым OW */
#define ONEWIRE_TIME_STD_RSTH_MIN      480000
#define ONEWIRE_TIME_OVDRV_RSTH_MIN     48000

/* --------------- 1-Wire Write Bit ------------------------------------------*/
/* Write Zero Low Time (W0L) */
#define ONEWIRE_TIME_STD_W0L_MIN        60000
#define ONEWIRE_TIME_STD_W0L_MAX       120000
#define ONEWIRE_TIME_OVDRV_W0L_MIN       6000
#define ONEWIRE_TIME_OVDRV_W0L_MAX      15500
/* Write One Low Time (W1L) */
#define ONEWIRE_TIME_STD_W1L_MIN         1000
#define ONEWIRE_TIME_STD_W1L_MAX        15000
#define ONEWIRE_TIME_OVDRV_W1L_MIN       1000
#define ONEWIRE_TIME_OVDRV_W1L_MAX       2000


/* --------------- 1-Wire Read Bit ------------------------------------------*/
/* Read Low Time (RL) */
#define ONEWIRE_TIME_STD_RL_MIN        5000
#define ONEWIRE_TIME_OVDRV_RL_MIN      1000
/* Read Sample Time (MSR) */
#define ONEWIRE_TIME_STD_MSR_MAX      15000
#define ONEWIRE_TIME_OVDRV_MSR_MAX     2000



#endif // ONEWIRE_TIMING_H
