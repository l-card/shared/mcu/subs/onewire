#ifndef ONEWIRE_ROMCMD_H
#define ONEWIRE_ROMCMD_H

#include "onewire_defs.h"

#define ONEWIRE_ROM_INFO_SIZE  8

typedef enum {
    ONWEWIRE_ROMCMD_READ        = 0x33,
    ONWEWIRE_ROMCMD_MATCH       = 0x55,
    ONWEWIRE_ROMCMD_SEARCH      = 0xF0,
    ONWEWIRE_ROMCMD_SKIP        = 0xCC,
    ONWEWIRE_ROMCMD_RESUME      = 0xA5,
    ONWEWIRE_ROMCMD_OVD_SKIP    = 0x3C,
    ONWEWIRE_ROMCMD_OVD_MATCH   = 0x69,
} t_onewire_rom_cmd;

/* содержимое ROM-памяти, для идентификации и выбора (при нескольких на шине) устройства */
typedef union {
    uint8_t data[ONEWIRE_ROM_INFO_SIZE];
    struct {
        uint8_t family_code;
        uint8_t serial[6];
        uint8_t crc8;
    };
} t_onewire_dev_rom;

void onewire_romcmd_start_read(int iface, t_onewire_dev_rom *rom);
t_onewire_err onewire_romcmd_check_romdata(const t_onewire_dev_rom *rom);
void onewire_romcmd_start_skip(int iface, t_onewire_speed new_speed);
void onewire_romcmd_start_match(int iface, const t_onewire_dev_rom *rom, t_onewire_speed cur_speed, t_onewire_speed new_speed);
void onewire_romcmd_start_resume(int iface, t_onewire_speed cur_speed);

#endif // ONEWIRE_ROMCMD_H
