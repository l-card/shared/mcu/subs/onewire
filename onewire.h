#ifndef ONEWIRE_H
#define ONEWIRE_H

#include "onewire_config.h"
#include "onewire_defs.h"
#include "onewire_ops.h"
#include "onewire_romcmd.h"
#include "onewire_dev.h"
#include "onewire_signaling.h"


static inline void onewire_init(void) {
    onewire_sig_init();
    onewire_devs_init();
    onewire_ops_init();
}

static inline void onewire_poll(void) {
    onewire_ops_poll();
}

static inline void onewire_close(void) {
    onewire_sig_close();
}


#endif // ONEWIRE_H
