ONEWIRE_MAKEFILE = $(abspath $(lastword $(MAKEFILE_LIST)))
ONEWIRE_DIR := $(dir $(ONEWIRE_MAKEFILE))


ONEWIRE_DEVS_DIR := $(ONEWIRE_DIR)/devs

ONEWIRE_SRC := $(ONEWIRE_DIR)/onewire_ops.c \
               $(ONEWIRE_DIR)/onewire_romcmd.c \
               $(ONEWIRE_DIR)/onewire_dev.c



ONEWIRE_INC_DIRS := $(ONEWIRE_DIR) $(ONEWIRE_DEVS_DIR)
